const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const ObjectId = Schema.ObjectId;
const timestamps = require('mongoose-timestamp');



let img = new Schema(
    {
      name: {
        type: String
      },
     
      img: {
        type: String
      },
      
        product_id: {
            type: ObjectId
          },
        
    },
   
    { collection: "images" }
  );

  img.plugin(timestamps);
module.exports = mongoose.model("images", img);