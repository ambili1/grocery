const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const ObjectId = Schema.ObjectId;
const timestamps = require('mongoose-timestamp');
let shop = new Schema(
  {
    name: {
      type: String
    },
   
    email: {
      type: String
    },
    phone: {
        type: String
      },

      password: {
        type: String
      }
  },
  { collection: "shop" }
);


shop.plugin(timestamps);
module.exports = mongoose.model("shop", shop);
