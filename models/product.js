const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const ObjectId = Schema.ObjectId;
const timestamps = require('mongoose-timestamp');


let product = new Schema(
    {
      name: {
        type: String
      },
     
      price: {
        type: String
      },
      description: {
          type: String
        },
  
        stock: {
          type: String
        },
        shop_id: {
            type: ObjectId
          }
         
    },
    { collection: "product" }
  );
  product.plugin(timestamps);
module.exports = mongoose.model("product", product);