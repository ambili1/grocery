var express = require("express");
const app = express();
const bcrypt = require("bcrypt");
var bodyParser = require("body-parser");
var router = express.Router();
var mongoose = require("mongoose");

const multer = require("multer");
session = require("express-session");
const jwt = require("jsonwebtoken");
var shop = require("../models/shop.js");
var product = require("../models/product.js");
var images = require("../models/images.js");

app.use("/uploads", express.static("../uploads"));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use(
  session({
    secret: "2C44-4D44-WppQ38S",
    resave: true,
    saveUninitialized: true,
  })
);

const accessTokenSecret = "myaccesstokensecretambily";

const saltRounds = 10;
const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, "./uploads/");
  },
  filename: function (request, file, callback) {
    callback(null, Date.now() + file.originalname);
  },
});

const upload = multer({
  storage: storage,
  limits: {
    fileSize: 1024 * 1024 * 5,
  },
});

/* authuntication middleware */
/*var auth = function (req, res, next) {
  if (req.session.logdin) {
    console.log(req.session);
    return next();
  } else return res.sendStatus(401);
};*/
const verifyToken = (req, res, next) => {
  const authtoken = req.header("auth-token");
  if (typeof authtoken == undefined || !authtoken || req.session.login == false)
    return res.status(401).json({ error: "Access denied" });
  try {
    const verified = jwt.verify(authtoken, accessTokenSecret);
    req.userid = verified.id;
    console.log(verified);
    next(); // to continue the flow
  } catch (err) {
    res.status(400).json({ error: "Token is not valid" });
  }
};

var productauth = function (req, res, next) {
  product.findById(req.query.id, function (err, post) {
    if (err) return next(err);

    if (post) {
      if (post.shop_id == req.userid) {
        return next();
      } else {
        return res.sendStatus(401);
      }
    } else {
      return res.sendStatus(401);
    }
  });
};

/* user registration*/
router.post("/register", async function (req, res, next) {
  if (!req.body.email || !req.body.password || !req.body.name) {
    res.status("400");
    res.send("Uncompleted details!");
  } else {
    await shop.find({ email: req.body.email }, function (err, user) {
      if (err) return next(err);
      if (user[0]) {
        res.send("This user  email alredy exist");
      } else {
        docs = req.body;
        console.log(docs.password);
        bcrypt.hash(req.body.password, saltRounds, (err, hash) => {
          if (err) return next(err);
          docs.password = hash;
          console.log(hash);

          shop.create(docs, function (err, post) {
            if (err) return next(err);
            res.status(200);
            res.send("SucessFul Registration");
            console.log(post);
          });
        });
      }
    });
  }
});

/* login */
router.post("/login", async function (req, res, next) {
  if (!req.body.email || !req.body.password) {
    res.status("400");
    res.send("Uncompleted details!");
  } else {
    await shop.findOne({ email: req.body.email }, function (err, docs) {
      if (err) return next(err);
      if (!docs) {
        res.send("invalid email");
      } else {
        bcrypt.compare(req.body.password, docs.password, function (err, hash) {
          if (err) return next(err);
          if (hash == true) {
            shp = docs;

            req.session.login = true;

            const token = jwt.sign(
              { id: docs._id, auth: true },
              accessTokenSecret,
              { expiresIn: 1200 }
            );

            res.status(200).send({ auth: true, token: token });
          } else {
            res.send("Wromg password");
          }
        });
      }
    });
  }
});

/* add product details */
router.post("/add", verifyToken, async function (req, res, next) {
  if (!req.body.name || !req.body.price) {
    res.send("Enter valid deatils");
  } else {
    pro_obj = {
      name: req.body.name,
      price: req.body.price,
      description: req.body.description,
      stock: req.body.stock,
      shop_id: req.userid,
    };
    await product.create(pro_obj, function (err, post) {
      if (err) return next(err);
      res.send("sucessfuly added product");
    });
  }
});

/* view product detils based on shop id */
router.get("/view", verifyToken, async function (req, res, next) {
  console.log(req.userid);
  await product.find({ shop_id: req.userid }, function (err, products) {
    if (err) return next(err);
    res.json(products);
  });
});

/* view a specific product */
router.get(
  "/viewOne",
  verifyToken,
  productauth,
  async function (req, res, next) {
    await product.findById(req.query.id, function (err, post) {
      if (err) return next(err);

      if (post.shop_id == req.userid) {
        res.status(200);
        res.json(post);
      } else {
        res.send("Unauthorized Product access");
      }
    });
  }
);

/* update stock of specic product */
router.put(
  "/updated_stock",
  verifyToken,
  productauth,
  async function (req, res, next) {
    await product.findByIdAndUpdate(
      req.query.id,
      { stock: req.body.stock },
      function (err, post) {
        if (err) return next(err);
        res.status(200);
        res.json("updated");
      }
    );
  }
);

/* update details of specific product */
router.put(
  "/updated",
  verifyToken,
  productauth,
  async function (req, res, next) {
    await product.findByIdAndUpdate(
      req.query.id,
      req.body,
      function (err, post) {
        if (err) return next(err);
        res.status(200);
        res.send(post);
      }
    );
  }
);

/* delete a specfic product based on id */
router.delete(
  "/delete",
  verifyToken,
  productauth,
  async function (req, res, next) {
    await product.findByIdAndRemove(req.query.id, function (err, post) {
      if (err) return next(err);
      res.status(200);
      res.send(post);
    });
  }
);

/* logout */
router.get("/logout", function (req, res) {
  req.session.login = false;

  res.send("logout sucessfuly");
});

/* view images of a specic product based on id */
router.get(
  "/view_images",
  verifyToken,
  productauth,
  async function (req, res, next) {
    await images.find({ product_id: req.query.id }, function (err, products) {
      if (err) return next(err);
      res.status(200);
      res.json(products);
    });
  }
);

/* add images of a specic product based on id */
router.post(
  "/add_image",
  verifyToken,
  productauth,
  upload.single("img"),
  async function (req, res, next) {
    var docm = {
      product_id: req.query.id,
      img: req.file.path + "" + req.file.filename,
    };

    await images.create(docm, function (err, post) {
      if (err) return next(err);
      res.status(200);
      res.send(post);
    });
  }
);

/* delete a specfic image based on id */
router.delete("/delete_image", verifyToken, async function (req, res, next) {
  if (req.query.id) {
    await images.findByIdAndRemove(req.query.id, function (err, post) {
      if (err) return next(err);
      res.status(200);
      res.json(post);
    });
  } else {
    res.send("give query param with id");
  }
});

module.exports = router;
