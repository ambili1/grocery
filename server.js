var express = require('express');
var path = require('path');
var bodyParser = require('body-parser');
var router = express.Router();
var app = express();
app.use('/uploads', express.static('./uploads'));
const mongoose = require("mongoose");
const multer = require("multer");
const jwt = require('jsonwebtoken');
var uri = "mongodb://localhost:27017/grocery";



var route = require('./router/route');

app.use(bodyParser.json());

app.use(bodyParser.urlencoded({ extended: true }));


session = require('express-session');
app.use(session({
    secret: '2C44-4D44-WppQ38S',
    resave: true,
    saveUninitialized: true
}));
 


mongoose.connect(uri, { useUnifiedTopology: true, useNewUrlParser: true ,useFindAndModify: false});
const connection = mongoose.connection;
connection.once("open", function() {
  console.log("MongoDB database connection established successfully");
})


app.use('/',route);

/* error handling */
app.use(function(err, req, res, next) {
  res.status(500);
  res.send("Oops, something went wrong.")
});
const PORT = 3000;
app.listen(PORT);
module.exports = app;
